﻿using CouponGenerator.Repository.Contracts;
using CouponGenerator.Repository.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CouponGenerator.App
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Number of Keys: ");
            string numberOfKeys = Console.ReadLine();
            IKeyRepository keyRep = new KeyRepository();
            if (!string.IsNullOrEmpty(numberOfKeys))
            {
                var keys = keyRep.Print(int.Parse(numberOfKeys));
                foreach (var key in keys)
                {
                    Console.WriteLine(key.Code);
                }
            }
            else
            {
                Console.Write("Number should not be null");
            }

            Console.ReadLine();
        }
    }
}
