﻿using CouponGenerator.Repository.Contracts;
using System.Collections.Generic;
using CouponGenerator.Model;
using CouponGenerator.Interface.Contracts;
using CouponGenerator.Interface;

namespace CouponGenerator.Repository.Implementation
{
    public class KeyRepository : IKeyRepository
    {
        private IKeyGenerator _keyGenerator;
        /// <summary>
        /// Initializes a new instance of the <see cref="KeyRepository"/> class.
        /// </summary>
        public KeyRepository()
        {
            _keyGenerator = KeyGeneratorManager.Instance;
        }

        /// <summary>
        /// Prints the specified number of keys.
        /// </summary>
        /// <param name="numberOfKeys">The number of keys.</param>
        /// <param name="lengthOfKey">The length of key.</param>
        /// <returns></returns>
        public List<Key> Print(int numberOfKeys, int lengthOfKey = 10, CaseSensitiveType type = CaseSensitiveType.UPPERCASE)
        {
            //check if numberOfKeys value is not null or empty in this case return null 
            List<Key> listOfKeys;
            //return null if numberOfKeys is less than 0
            if (numberOfKeys <= 0)
            {
                return null;
            }
            else
            {
                //create an instance of new list of key model
                listOfKeys = new List<Key>();
                //Generate Randon keys 
                while (numberOfKeys > 0)
                {
                    Key key = _keyGenerator.GenerateRandomKey(lengthOfKey, type);
                    listOfKeys.Add(key);
                    //each key generation will decrement numberOfKeys
                    numberOfKeys--;
                }
            }
            return listOfKeys;
        }
    }
}