﻿using CouponGenerator.Model;
using System.Collections.Generic;

namespace CouponGenerator.Repository.Contracts
{
    public interface IKeyRepository
    {
        List<Key> Print(int numberOfKeys, int lengthOfKey = 10, CaseSensitiveType type = CaseSensitiveType.UPPERCASE);
    }
}