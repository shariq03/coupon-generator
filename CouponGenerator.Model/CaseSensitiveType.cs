﻿namespace CouponGenerator.Model
{
    public enum CaseSensitiveType
    {
        NONE,
        UPPERCASE,
        LOWERCASE        
    }
}
