﻿using CouponGenerator.Interface.Contracts;
using CouponGenerator.Interface.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CouponGenerator.Interface
{
    public class KeyGeneratorManager
    {
        private static IKeyGenerator _instance;
        /// <summary>
        /// Returns the Singleton instance of <see cref="KeyGenerator"/>
        /// </summary>
        /// <value>
        /// A lazily initialized singleton value
        /// </value>
        public static IKeyGenerator Instance
        {

            get
            {
                if (_instance == null)
                {
                    _instance = new KeyGenerator();
                }
                return _instance;
            }
        }
    }
}
