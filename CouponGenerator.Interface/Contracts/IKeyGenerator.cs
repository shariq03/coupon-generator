﻿using CouponGenerator.Model;

namespace CouponGenerator.Interface.Contracts
{
    public interface IKeyGenerator
    {
        /// <summary>
        /// Generates the random key.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <returns></returns>
        Key GenerateRandomKey(int length, CaseSensitiveType type = CaseSensitiveType.UPPERCASE);
    }
}
