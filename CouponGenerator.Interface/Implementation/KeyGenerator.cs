﻿using CouponGenerator.Interface.Contracts;
using System.Text;
using CouponGenerator.Model;
using System.Security.Cryptography;

namespace CouponGenerator.Interface.Implementation
{
    public class KeyGenerator : IKeyGenerator
    {

        /// <summary>
        /// Generates a Cryptographicallty secure random key.
        /// </summary>
        /// <param name="length">The length.</param>
        /// <returns></returns>
        /// <remarks>
        /// For a 256 bit key, pass the length as 32 (32 bytes * 8 bits = 256 bits)
        /// </remarks>
        public Key GenerateRandomKey(int length, CaseSensitiveType type)
        {
            Key key = new Key();
            char[] chars = new char[62];
            chars =
            "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890".ToCharArray();
            byte[] data = new byte[1];
            using (RNGCryptoServiceProvider crypto = new RNGCryptoServiceProvider())
            {
                crypto.GetNonZeroBytes(data);
                data = new byte[length];
                crypto.GetNonZeroBytes(data);
            }
            StringBuilder result = new StringBuilder(length);
            foreach (byte b in data)
            {
                result.Append(chars[b % (chars.Length)]);
            }

            //consider case sensitive values 
            if (type == CaseSensitiveType.UPPERCASE)
            {
                //convert key to uppercase  
                key.Code = result.ToString().ToUpper();
            }
            else if (type == CaseSensitiveType.LOWERCASE)
            {
                //convert key to lowercase 
                key.Code = result.ToString().ToLower();
            }
            else
            {
                //do nothing
                key.Code = result.ToString();
            }


            return key;
        }

    }
}
