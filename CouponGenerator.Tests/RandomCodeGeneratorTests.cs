﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CouponGenerator.Repository.Contracts;
using CouponGenerator.Repository.Implementation;

namespace CouponGenerator.Tests
{
    [TestClass]
    public class RandomCodeGeneratorTests
    {
        [TestMethod]
        public void GenerateRandomKey_Returns_NumbersOfExpectedRandomKeys()
        {

            IKeyRepository keyRepository = new KeyRepository();
            var keys = keyRepository.Print(100);
            Assert.AreEqual(100, keys.Count);
        }

        [TestMethod]
        public void GenerateRandomKey_Returns_KeyOf_ExpectedLength()
        {
            IKeyRepository keyRepository = new KeyRepository();
            var keys = keyRepository.Print(1, 16);
            Assert.AreEqual(16, keys[0].Code.Length);
        }

        [TestMethod]
        public void GenerateRandomKey_Returns_NumbersOfExpectedRandomKeys_UpperCase()
        {
            IKeyRepository keyRepository = new KeyRepository();
            var keys = keyRepository.Print(numberOfKeys: 1, type: Model.CaseSensitiveType.UPPERCASE);
            var result = keys[0].Code.ToUpper();
            Assert.AreEqual(result, keys[0].Code);
        }

        [TestMethod]
        public void GenerateRandomKey_Returns_NumbersOfExpectedRandomKeys_LowerCase()
        {
            IKeyRepository keyRepository = new KeyRepository();
            var keys = keyRepository.Print(numberOfKeys: 1, type: Model.CaseSensitiveType.LOWERCASE);
            var result = keys[0].Code.ToLower();
            Assert.AreEqual(result, keys[0].Code);
        }

        [TestMethod]
        public void GenerateRandomKey_Returns_NumbersOfExpectedRandomKeys_None()
        {
            IKeyRepository keyRepository = new KeyRepository();
            var keys = keyRepository.Print(numberOfKeys: 1, type: Model.CaseSensitiveType.NONE);
            var result = keys[0].Code;
            Assert.AreEqual(result, keys[0].Code);
        }

        [TestMethod]
        public void GenerateRandomKey_Returns_NumbersOfExpectedRandomKeys_ExpectedLength_UPPERCASE()
        {
            IKeyRepository keyRepository = new KeyRepository();
            var keys = keyRepository.Print(1, 10, Model.CaseSensitiveType.UPPERCASE);
            var result = keys[0].Code.ToUpper();
            Assert.AreEqual(result, keys[0].Code);
        }
    }
}
