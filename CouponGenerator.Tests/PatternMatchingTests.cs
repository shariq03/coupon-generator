﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using CouponGenerator.Repository.Contracts;
using CouponGenerator.Repository.Implementation;
using System.Linq;

namespace CouponGenerator.Tests
{
    [TestClass]
    public class PatternMatchingTests
    {
        [TestMethod]
        public void PatternMatching_CheckForDuplicateKeys()
        {
            IKeyRepository keyRep = new KeyRepository();
            var keys = keyRep.Print(100000);
            bool keyNotFound = true;
            //this will loop though each key and list of keys 
            //if key has been repeated it keyNotFound return false and it fail the Test
            //bit time consuming but this could be refactored it is talking 3mins to match keys
            foreach (var key in keys)
            {
                var found = (from k in keys
                             where k.Code == key.Code
                             select k).ToList();

                if (found.Count > 1)
                {
                    keyNotFound = false;
                    break;
                }
            }
            Assert.AreEqual(true, keyNotFound);
        }
    }
}
